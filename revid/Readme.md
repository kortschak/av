# Readme

revid is a testbed for re-muxing and re-directing video streams as
MPEG-TS over various protocols.

# Description

The mode (-m) determine the mode of operation:

* h = send HTTP (as a POST)
* u = send UDP
* r = send RTP
* f = write to /tmp files
* d = inspect packets and dump to screen

Flags (-f) determine video filtering and other actions.

For example, to send as raw UDP to <PORT> on the current host, passing the video and audio as is:

revid -i <RTSP_URL> -m u -o udp://0.0.0.0:<PORT>

Or, to post as HTTP to <HTTP_URL>, fixing PTS and dropping the audio along the way:

revid -i <RTSP_URL> -m h -f 3 -o <HTTP_URL>

Note that revid appends the size of the video to the URL to supply a query param.
Append a ? to your <URL> if you don't need it

List of flags:

* filterFixPTS        = 0x0001
* filterDropAudio     = 0x0002
* filterScale640      = 0x0004
* filterScale320      = 0x0008
* filterFixContinuity = 0x0010
* dumpProgramInfo     = 0x0100
* dumpPacketStats     = 0x0200
* dumpPacketHeader    = 0x0400
* dumpPacketPayload   = 0x0800

Common flag combos:

* 3: fix pts and drop audio
* 7: fix pts, drop audo and scale 640
* 17: fix pts and fix continuity
* 256: dump program info
* 512: dump packet stats
* 513: fix pts, plus above
* 529: fix pts and fix continuity, plus above

# Errors

If you see "Error reading from ffmpeg: EOF" that means ffmpeg has
crashed for some reason, usually because of a bad parameter. Copy and
paste the ffmpeg command line into a terminal to see what is
happening.

RTSP feeds from certain cameras (such as TechView ones) do not
generate presentation timestamps (PTS), resulting in errors such as
the following:

* [mpegts @ 0xX] Timestamps are unset in a packet for stream 0...
* [mpegts @ 0xX] first pts value must be set

This can be fixed  with an ffmpeg video filter (specified by flag 0x0001).
Another issue is that MPEG-TS continuity counters may not be continuous.
You can fix this with the fix continuity flag (0x0010).

FFmpeg will also complain if doesn't have the necessary audio codec
installed. If so, you can drop the audio (flag 0x0002).

# MPEG-TS Notes

MPEG2-TS stream clocks (PCR, PTS, and DTS) all have units of 1/90000
second and header fields are read as big endian (like most protocols).

* TEI = Transport Error Indicator
* PUSI = Payload Unit Start Indicator
* TP = Transport Priority
* TCS = Transport Scrambling Control
* AFC = Adapation Field Control
* CC = Continuity Counter (incremented per PID wen payload present)
* AFL = Adapation Field Length
* PCR = Program Clock Reference

# Dependencies

revid uses ffmpeg for video remuxing.
See [Ffmepg filters](https://ffmpeg.org/ffmpeg-filters.html).

revid also uses [Comcast's gots package](https://github.com/Comcast/gots).

# Author

Alan Noble <anoble@gmail.com>

# License

Revid is Copyright (C) 2017 the Australian Ocean Lab (AusOcean).

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
or more details.

You should have received a copy of the GNU General Public License
along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses/).
