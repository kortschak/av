/*
NAME
  revid_test.go

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon@ausocean.org>

LICENSE
  revid_test.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package revid

import (
	"testing"
	"time"
)

/*
// Test revidInst with a file input
func TestFileInput(t *testing.T){
	config := Config{
		Input: File,
		InputFileName: "testInput.h264",
		Output: File,
		OutputFileName: "output/TestFileAsInput.ts",
	}
	revidInst, err := NewRevidInstance(config)
	if err != nil {
		t.Errorf("Should not have got error!: %v\n", err.Error())
    return
	}
	revidInst.Start()
	time.Sleep(100*time.Second)
	revidInst.Stop()
}

// Test revidInst with a Raspivid h264 input
func TestRaspividH264Input(t *testing.T){
  config := Config{
    Input: Raspivid,
    Output: File,
    OutputFileName: "output/TestRaspividOutput.ts",
    Width: "1280",
    Height: "720",
    FrameRate: "25",
  }
  revidInst, err := NewRevidInstance(config)
  if err != nil {
    t.Errorf("Should not have got an error!")
    return
  }
  revidInst.Start()
  time.Sleep(100*time.Second)
  revidInst.Stop()
}


// Test revidInst with a raspivid mjpeg input
func TestRaspividMJPEGInput(t *testing.T){
  config := Config{
    Input: Raspivid,
    InputCodec: Mjpeg,
    Output: File,
    OutputFileName: "output/TestMjpeg.mjpeg",
    Width: "1280",
    Bitrate: "10000000",
    Height: "720",
    FrameRate: "25",
  }
  revidInst, err := NewRevidInstance(config)
  if err != nil {
    t.Errorf("Should not of have got an error!: %v\n", err.Error())
    return
  }
  revidInst.Start()
  time.Sleep(20*time.Second)
  revidInst.Stop()
}



// Test revidInst with rtmp output
func TestRtmpOutput(t *testing.T){
  config := Config{
    Input: File,
    InputFileName: "testInput.h264",
    InputCodec: H264,
    Output: Rtmp,
    RtmpUrl: "rtmp://a.rtmp.youtube.com/live2/w44c-mkuu-aezg-ceb1",
    Width: "1280",
    Height: "720",
    FrameRate: "25",
    Packetization: None,
    FramesPerClip: 1,
  }
  revidInst, err := NewRevidInstance(config)
  if err != nil {
    t.Errorf("Should not of have got an error!: %v\n", err.Error())
    return
  }
  revidInst.Start()
  time.Sleep(120*time.Second)
  revidInst.Stop()
}



// Test h264 inputfile to flv output files
func TestFlvOutputFile(t *testing.T) {
	config := Config{
		Input:          File,
		InputFileName:  "betterInput.h264",
		InputCodec:     H264,
		Output:         File,
		OutputFileName: "saxonOut.flv",
		Packetization:  Flv,
		FrameRate:      "25",
	}
	revidInst, err := NewRevidInstance(config)
	if err != nil {
		t.Errorf("Should not of have got an error!: %v\n", err.Error())
		return
	}
	revidInst.Start()
	time.Sleep(30 * time.Second)
	revidInst.Stop()
}
*/

/*
// Test h264 inputfile to flv format into rtmp using librtmp c wrapper
func TestRtmpOutputUsingLibRtmp(t *testing.T){
	config := Config{
		Input:          File,
		InputFileName:  "input/betterInput.h264",
		InputCodec:     H264,
		Output:         Rtmp,
		RtmpMethod: LibRtmp,
		RtmpUrl: "rtmp://a.rtmp.youtube.com/live2/w44c-mkuu-aezg-ceb1",
		FramesPerClip: 1,
		Packetization:  Flv,
		FrameRate:      "25",
	}
	revidInst, err := NewRevidInstance(config)
	if err != nil {
		t.Errorf("Should not of have got an error!: %v\n", err.Error())
		return
	}
	revidInst.Start()
	time.Sleep(120*time.Second)
	revidInst.Stop()
}
*/

// Test revidInst with a Raspivid h264 input
func TestRaspividToRtmp(t *testing.T) {
	config := Config{
		Input:            Raspivid,
		Output:           Rtmp,
		RtmpMethod:       LibRtmp,
		QuantizationMode: QuantizationOff,
		RtmpUrl:          "rtmp://a.rtmp.youtube.com/live2/w44c-mkuu-aezg-ceb1",
		Bitrate:          "500000",
		FramesPerClip:    1,
		Packetization:    Flv,
		FrameRate:        "25",
	}
	revidInst, err := NewRevid(config)
	if err != nil {
		t.Errorf("Should not have got an error!")
		return
	}
	revidInst.Start()
	time.Sleep(43200 * time.Second)
	revidInst.Stop()
}
