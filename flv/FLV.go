/*
NAME
  FLV.go

DESCRIPTION
  See Readme.md

AUTHORS
	Saxon A. Nelson-Milton <saxon@ausocean.org>

LICENSE
  FLV.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package flv

import "bitbucket.org/ausocean/av/tools"

const (
	headerLength    = 72
	version         = 0x01
	maxVideoTagSize = 10000
	maxAudioTagSize = 10000
)

const (
	VideoTagType         = 9
	AudioTagType         = 8
	KeyFrameType         = 1
	InterFrameType       = 2
	H264                 = 7
	AVCNALU              = 1
	SequenceHeader       = 0
	DataHeaderLength     = 5
	NoTimestampExtension = 0
	AACAudioFormat       = 10
	PCMAudioFormat       = 0
)

var flvHeaderCode = []byte{0x46, 0x4C, 0x56}

type Header struct {
	AudioFlag bool
	VideoFlag bool
}

func btb(b bool) byte {
	return tools.BoolToByte(b)
}

func (h *Header) ToByteSlice() (output []byte) {
	output = make([]byte, 0, headerLength)
	output = append(output, flvHeaderCode...)
	output = append(output, []byte{
		version,
		0x00 | btb(h.AudioFlag)<<2 | btb(h.VideoFlag),
		0x00, 0x00, 0x00, byte(9),
	}...)
	return
}
