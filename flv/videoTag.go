/*
NAME
  videoTag.go

DESCRIPTION
  See Readme.md

AUTHORS
	Saxon Nelson-Milton <saxon@ausocean.org>

LICENSE
  videoTag.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package flv

type VideoTag struct {
	TagType           uint8
	DataSize          uint32
	Timestamp         uint32
	TimestampExtended uint32
	FrameType         byte
	Codec             byte
	PacketType        byte
	CompositionTime   uint32
	Data              []byte
	PrevTagSize       uint32
}

func (t *VideoTag) ToByteSlice() (output []byte) {
	output = make([]byte, 0, maxVideoTagSize)
	output = append(output, []byte{
		byte(t.TagType),
		byte(t.DataSize >> 16),
		byte(t.DataSize >> 8),
		byte(t.DataSize),
		byte(t.Timestamp >> 16),
		byte(t.Timestamp >> 8),
		byte(t.Timestamp),
		byte(t.TimestampExtended),
		0x00,
		0x00,
		0x00,
		0x00 | byte(t.FrameType<<4) | byte(t.Codec),
		t.PacketType,
		byte(t.CompositionTime >> 16),
		byte(t.CompositionTime >> 8),
		byte(t.CompositionTime),
	}...)
	output = append(output, t.Data...)
	output = append(output, []byte{
		byte(t.PrevTagSize >> 24),
		byte(t.PrevTagSize >> 16),
		byte(t.PrevTagSize >> 8),
		byte(t.PrevTagSize),
	}...)
	return
}
