# Readme

av is a collection of tools and packages written in Go for audio-video processing.

# Authors
Alan Noble
Saxon A. Nelson-Milton <saxon.milton@gmail.com>

# Description

* revid: a tool for re-muxing and re-directing video streams.
* RingBuffer: a package that implements a ring buffer with concurrency control.

# License

Copyright (C) 2017 the Australian Ocean Lab (AusOcean).

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
or more details.

You should have received a copy of the GNU General Public License
along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses/).
