/*
NAME
  RingBuffer_test.go - a test suite adopting the golang testing library to test functionality of the
  RingBuffer structure

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon.milton@gmail.com>

LICENSE
  RingBuffer_test.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package ringbuffer

import (
	"fmt"
	"testing"
	"time"
)

// Parameters used over the testing
const (
	testBufferSize            = 10
	testElementSize           = 1
	testOverFlowSize          = 2
	testDataSize              = 1
	testConcurrencyBufferSize = 1000
)

// Types of actions, assertions and test ends
const (
	testWrite       = 0
	testDoneWriting = 1
	testRead        = 2
	testDoneReading = 3
	testEnd         = 4
	assertNoError   = 5
	assertError     = 6
)

// Strings to help with test fail messages
const (
	noErrorFailMessage    = "Should have got error!"
	falseErrorFailMessage = "Should not have got error: %v"
	atMessage             = " Series: %v"
)

// Pointer to the ringbuffer we will be testing
var rb *ringBuffer

// Call when the a test array has the wrong format
func wrongFmt() {
	fmt.Println("Test has wrong format!")
}

// Test that the Make method correctly allocates memory for arbitrary buffer
// size of 10 and arbitrary element size of 10
func TestMake(t *testing.T) {
	rb = NewRingBuffer(testBufferSize, testElementSize)
	if len(rb.dataMemory) != testBufferSize {
		t.Errorf("Len of buffer is wrong!")
	}
	if len(rb.dataMemory[0]) != testElementSize {
		t.Errorf("Len of individual element is wrong!")
	}
}

// Format: { <size of uffer>, <type of check>, <action>, ..., <action>, <test end> }
var tests = [][]int{
	{testBufferSize, assertError, testDoneWriting, testEnd},
	{testBufferSize, assertNoError, testWrite, testDoneWriting, testEnd},
	{testBufferSize, assertError, testDoneReading, testEnd},
	{testBufferSize, assertError, testRead, testEnd},
	{testBufferSize, assertError, testWrite, testWrite, testEnd},
	{testBufferSize, assertNoError, testWrite, testDoneWriting, testRead,
		testDoneReading, testEnd},
	{testBufferSize, assertNoError, testWrite, testDoneWriting, testWrite,
		testDoneWriting, testRead, testDoneReading, testRead, testDoneReading,
		testEnd},
	{testBufferSize, assertError, testWrite, testDoneWriting, testRead,
		testDoneReading, testRead, testDoneReading, testEnd},
	{testOverFlowSize, assertError, testWrite, testDoneWriting, testWrite, testDoneWriting,
		testWrite, testDoneWriting, testEnd},
	{testOverFlowSize, assertNoError, testWrite, testDoneWriting, testWrite, testDoneWriting,
		testRead, testDoneReading, testWrite, testDoneWriting, testEnd},
	{testOverFlowSize, assertError, testWrite, testDoneWriting, testWrite, testDoneWriting,
		testRead, testDoneReading, testWrite, testDoneWriting, testWrite,
		testDoneWriting, testEnd},
	{testBufferSize, assertError, testWrite, testDoneWriting, testWrite,
		testDoneWriting, testRead, testRead, testEnd},
}

// Tests series of actions performed by the ring buffer defined in array tests
func TestAllSorts(t *testing.T) {
	for i := range tests {
		rb = NewRingBuffer(tests[i][0], testElementSize)
		var err error
		for j := 2; tests[i][j] != testEnd; j++ {
			var newErr error
			switch tests[i][j] {
			case testWrite:
				_, newErr = rb.Get()
			case testDoneWriting:
				newErr = rb.DoneWriting(testDataSize)
			case testRead:
				_, newErr = rb.Read()
			case testDoneReading:
				newErr = rb.DoneReading()
			default:
				wrongFmt()
			}
			if err == nil {
				err = newErr
			}
		}
		switch tests[i][1] {
		case assertError:
			if err == nil {
				t.Errorf(noErrorFailMessage+atMessage, i)
			}
		case assertNoError:
			if err != nil {
				t.Errorf(falseErrorFailMessage+atMessage, err, i)
			}
		default:
			wrongFmt()
		}
	}
}

// Let's see if we can do concurrency. (tip: run as: go test -race) to see
// any data race issues. We will continously write and read at concurrently
// time for a second
func TestConcurrency1(t *testing.T) {
	rb = NewRingBuffer(testConcurrencyBufferSize, testElementSize)
	go func() {
		for i := 0; i < 100; i++ {
			if data, err1 := rb.Read(); data != nil {
				err2 := rb.DoneReading()
				if err1 != nil {
					t.Errorf(falseErrorFailMessage, err1)
				}
				if err2 != nil {
					t.Errorf(falseErrorFailMessage, err2)
				}
			}
		}
	}()
	go func() {
		for i := 0; i < 100; i++ {
			_, err1 := rb.Get()
			err2 := rb.DoneWriting(testDataSize)
			if err1 != nil {
				t.Errorf(falseErrorFailMessage, err1)
			}
			if err2 != nil {
				t.Errorf(falseErrorFailMessage, err2)
			}
		}
	}()
	time.Sleep(1000 * time.Millisecond)
}
