/*
NAME
  MpegTs.go - provides a data structure intended to encapsulate the properties
  of an MpegTs packet.

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon.milton@gmail.com>

LICENSE
  MpegTs.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package mpegts

import "testing"

// Just ensure that we can create a byte slice with a mpegts packet correctly
func TestMpegTsToByteSlice(t *testing.T) {
	payload := []byte{0x56, 0xA2, 0x78, 0x89, 0x67}
	pcr := 100000 // => 100000
	stuffing := make([]byte, 171)
	for i := range stuffing {
		stuffing[i] = 0xFF
	}
	tsPkt := MpegTsPacket{
		PUSI:    true,
		PID:     uint16(256),
		AFC:     byte(3),
		AFL:     7 + 171,
		CC:      byte(6),
		PCRF:    true,
		PCR:     uint64(pcr),
		Stuff:   stuffing,
		Payload: payload,
	}
	expectedOutput := []byte{0x47, 0x41, 0x00, 0x36, byte(178), 0x10}
	for i := 40; i >= 0; i -= 8 {
		expectedOutput = append(expectedOutput, byte(pcr>>uint(i)))
	}
	for i := 0; i < 171; i++ {
		expectedOutput = append(expectedOutput, 0xFF)
	}
	expectedOutput = append(expectedOutput, payload...)
	tsPktAsByteSlice, err := tsPkt.ToByteSlice()
	if err != nil {
		t.Errorf("Should not have got error!: %v", err)
	}
	for i := 0; i < 188; i++ {
		if tsPktAsByteSlice[i] != expectedOutput[i] {
			t.Errorf("Conversion to byte slice bad! Byte: %v Wanted: %v Got: %v", i, expectedOutput[i], tsPktAsByteSlice[i])
		}
	}
}
