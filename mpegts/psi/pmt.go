/*
NAME
  revid - a testbed for re-muxing and re-directing video streams as MPEG-TS over various protocols.

DESCRIPTION
  See Readme.md

AUTHOR
  Alan Noble <anoble@gmail.com>

LICENSE
  revid is Copyright (C) 2017 Alan Noble.

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package psi

type PMT struct {
	PF      byte   // Point field
	PFB     []byte // pointer filler bytes
	TableID byte   // Table ID
	SSI     bool   // Sectiopn syntax indicator (1 for PAT, PMT, CAT)
	PB      bool   // Private bit (0 for PAT, PMT, CAT)
	SL      uint16 // Section length
	TIE     uint16 // Table ID extension
	Version byte   // Version number
	CNI     bool   // Current/next indicator
	Section byte   // Section number
	LSN     byte   // Last section number

}

func (p *PMT) ToByteSlice() (output []byte) {
	return
}
