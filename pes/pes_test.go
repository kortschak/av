/*
NAME
  MpegTs.go - provides a data structure intended to encapsulate the properties
  of an MpegTs packet.

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon.milton@gmail.com>

LICENSE
  MpegTs.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package pes

import (
	"testing"
)

const (
	dataLength = 3 // bytes
)

func TestPesToByteSlice(t *testing.T) {
	pesPkt := PESPacket{
		StreamID:     0xE0, // StreamID
		PDI:          byte(2),
		PTS:          100000,
		HeaderLength: byte(10),
		Stuff:        []byte{0xFF, 0xFF},
		Data:         []byte{0xEA, 0x4B, 0x12},
	}
	pesExpectedOutput := []byte{
		0x00,     // packet start code prefix byte 1
		0x00,     // packet start code prefix byte 2
		0x01,     // packet start code prefix byte 3
		0xE0,     // stream ID
		0x00,     // PES Packet length byte 1
		0x00,     // PES packet length byte 2
		0x80,     // Marker bits,ScramblingControl, Priority, DAI, Copyright, Original
		0x80,     // PDI, ESCR, ESRate, DSMTrickMode, ACI, CRC, Ext
		byte(10), // header length
		0x21,     // PCR byte 1
		0x00,     // pcr byte 2
		0x07,     // pcr byte 3
		0x0D,     // pcr byte 4
		0x41,     // pcr byte 5
		0xFF,     // Stuffing byte 1
		0xFF,     // stuffing byte 3
		0xEA,     // data byte 1
		0x4B,     // data byte 2
		0x12,     // data byte 3
	}
	pesPktAsByteSlice := pesPkt.ToByteSlice()
	for ii := range pesPktAsByteSlice {
		if pesPktAsByteSlice[ii] != pesExpectedOutput[ii] {
			t.Errorf("Conversion to byte slice bad! Byte: %v Wanted: %v Got: %v",
				ii, pesExpectedOutput[ii], pesPktAsByteSlice[ii])
		}
	}
}
