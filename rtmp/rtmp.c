/*
NAME
  rtmp.c

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon@ausocean.org>
  Dan Kortschak <dan@ausocean.org>

LICENSE
  RTMPWrapper.c is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see http://www.gnu.org/licenses.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <rtmp_sys.h>
#include <log.h>
#include <rtmp.h>

RTMP* start_session(RTMP* rtmp, char* url, uint connect_timeout) {
	printf("RTMP url: %s\n", url);
	rtmp = RTMP_Alloc();
	RTMP_Init(rtmp);
	rtmp->Link.timeout = connect_timeout;
	if (!RTMP_SetupURL(rtmp, url)) {
		printf("Can't setup url!\n");
		RTMP_Close(rtmp);
		RTMP_Free(rtmp);
		return NULL;
	}

	RTMP_EnableWrite(rtmp);
	RTMP_SetBufferMS(rtmp, 3600 * 1000);
	if (!RTMP_Connect(rtmp, NULL)) {
		printf("RTMP can't connect!\n");
		RTMP_Close(rtmp);
		RTMP_Free(rtmp);
		return NULL;
	}

	if (!RTMP_ConnectStream(rtmp, 0)) {
		printf("RTMP can't connect stream!\n");
		RTMP_Close(rtmp);
		RTMP_Free(rtmp);
		return NULL;
	}

	return rtmp;
}

unsigned int write_frame(RTMP* rtmp, char* data, uint data_length) {
	char* dataForC = malloc(data_length);
	memcpy(dataForC,data,data_length);
	if (!RTMP_IsConnected(rtmp)) {
		printf("RTMP is not connected!\n");
		free(dataForC);
		return 1;
	}

	if (!RTMP_Write(rtmp, (const char*)data, data_length)) {
		printf("RTMP write error!\n");
		free(dataForC);
		return 1;
	}

	free(dataForC);

	return 0;
}

unsigned int end_session(RTMP* rtmp) {
	if (rtmp == NULL) {
		printf("Tried to end RTMP session, but not allocated yet!\n");
		return 1;
	}

	RTMP_Close(rtmp);
	RTMP_Free(rtmp);
	return 0;
}
