/*
NAME
  MpegTs.go - provides a data structure intended to encapsulate the properties
  of an MpegTs packet.

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon.milton@gmail.com>

LICENSE
  MpegTs.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package tools

import (
	"testing"
)

func TestH264Parsing(t *testing.T) {
	// Using file
	/*
		file, err := os.Open(fileName)
		if err != nil {
			panic("Could not open file!")
			return
		}
		stats, err := file.Stat()
		if err != nil {
			panic("Could not get file stats!")
		}
		buffer := make([]byte, stats.Size())
		_, err = file.Read(buffer)
		if err != nil {
			panic("Could not read file!")
		}
	*/
	// straight from buffer
	someData := []byte{
		0, 0, 1, 7, 59, 100, 45, 82, 93, 0, 0, 1, 8, 23, 78, 65, 0, 0, 1, 6, 45, 34, 23, 3, 2, 0, 0, 1, 5, 3, 4, 5,
		56, 76, 4, 234, 78, 65, 34, 34, 43, 0, 0, 1, 7, 67, 10, 45, 8, 93, 0, 0, 1, 8, 23, 7, 5, 0, 0, 1, 6,
		4, 34, 2, 3, 2, 0, 0, 1, 1, 3, 4, 5, 5, 76, 4, 234, 78, 65, 34, 34, 43, 45,
	}
	nalAccess1 := []byte{
		0, 0, 1, 9, 240, 0, 0, 1, 7, 59, 100, 45, 82, 93, 0, 0, 1, 8, 23, 78, 65, 0, 0, 1, 6, 45, 34, 23, 3, 2, 0, 0, 1, 5, 3, 4, 5,
		56, 76, 4, 234, 78, 65, 34, 34, 43,
	}
	nalAccess2 := []byte{
		0, 0, 1, 9, 240, 0, 0, 1, 7, 67, 10, 45, 8, 93, 0, 0, 1, 8, 23, 7, 5, 0, 0, 1, 6,
		4, 34, 2, 3, 2, 0, 0, 1, 1, 3, 4, 5, 5, 76, 4, 234, 78, 65, 34, 34, 43, 45,
	}
	aChannel := make(chan []byte, 10)
	var nalAccessChan chan<- []byte
	nalAccessChan = aChannel
	go ParseH264Buffer(someData, nalAccessChan)
	anAccessUnit := <-aChannel
	for i := range anAccessUnit {
		if anAccessUnit[i] != nalAccess1[i] {
			t.Errorf("Should have been equal!")
		}
	}
	anAccessUnit = <-aChannel
	for i := range anAccessUnit {
		if anAccessUnit[i] != nalAccess2[i] {
			t.Errorf("Should have been equal!")
		}
	}
}
