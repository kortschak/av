/*
NAME
  Parser.go

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon.milton@gmail.com>

LICENSE
  RtpToTsConverter.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package parser

import (
	"log"
	"sync"
)

// h264 consts
const (
	acceptedLength = 1000
)

var (
	Info  *log.Logger
	mutex *sync.Mutex
)

type Parser interface {
	Stop()
	Start()
	GetInputChan() chan byte
	GetOutputChan() chan []byte
	SetOutputChan(achan chan []byte)
	SetDelay(delay uint)
}
