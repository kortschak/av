/*
NAME
  MJPEGParser.go

DESCRIPTION
  See Readme.md

AUTHOR
  Saxon Nelson-Milton <saxon@ausocean.org>

LICENSE
  MJPEGParser.go is Copyright (C) 2017 the Australian Ocean Lab (AusOcean)

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

package parser

const (
	frameStartCode = 0xD8
)

type mjpegParser struct {
	inputBuffer         []byte
	isParsing           bool
	parserOutputChanRef chan []byte
	userOutputChanRef   chan []byte
	inputChan           chan byte
	delay               uint
}

func NewMJPEGParser(inputChanLen int) (p *mjpegParser) {
	p = new(mjpegParser)
	p.isParsing = true
	p.inputChan = make(chan byte, inputChanLen)
	return
}

func (p *mjpegParser) Stop() {
	p.isParsing = false
}

func (p *mjpegParser) Start() {
	go p.parse()
}

func (p *mjpegParser) SetDelay(delay uint) {
	p.delay = delay
}

func (p *mjpegParser) GetInputChan() chan byte {
	return p.inputChan
}

func (p *mjpegParser) GetOutputChan() chan []byte {
	return p.userOutputChanRef
}

func (p *mjpegParser) SetOutputChan(aChan chan []byte) {
	p.parserOutputChanRef = aChan
	p.userOutputChanRef = aChan
}

func (p *mjpegParser) parse() {
	var outputBuffer []byte
	for p.isParsing {
		aByte := <-p.inputChan
		outputBuffer = append(outputBuffer, aByte)
		if aByte == 0xFF && len(outputBuffer) != 0 {
			aByte := <-p.inputChan
			outputBuffer = append(outputBuffer, aByte)
			if aByte == frameStartCode {
				p.parserOutputChanRef <- outputBuffer[:len(outputBuffer)-2]
				outputBuffer = outputBuffer[len(outputBuffer)-2:]
			}
		}
	}
}
