package parser

import (
	"fmt"
	"os"
	"strconv"
	"testing"
)

const (
	testInputFileName = "testInput/testInput.avi"
)

func TestMJPEGParser(t *testing.T) {
	fmt.Println("Opening input file!")
	// Open the input file
	inputFile, err := os.Open(testInputFileName)
	if err != nil {
		t.Errorf("Should not have got error opening file!")
	}
	fmt.Println("Getting file stats!")
	stats, err := inputFile.Stat()
	if err != nil {
		t.Errorf("Could not get input file stats!")
		return
	}
	fmt.Println("Creating space for file data!")
	data := make([]byte, stats.Size())
	_, err = inputFile.Read(data)
	if err != nil {
		t.Errorf("Should not have got read error!")
		return
	}
	fmt.Println("Creating parser!")
	parser := NewMJPEGParser(len(data) + 1)
	parser.SetOutputChan(make(chan []byte, 10000))
	parser.Start()
	fmt.Printf("len(data): %v\n", len(data))
	for i := range data {
		parser.GetInputChan() <- data[i]
	}
	fmt.Println("Writing jpegs to files!")
	for i := 0; len(parser.GetOutputChan()) > 0; i++ {
		// Open a new output file
		outputFile, err := os.Create("testOutput/image" + strconv.Itoa(i) + ".jpeg")
		if err != nil {
			t.Errorf("Should not have got error creating output file!")
			return
		}
		outputFile.Write(<-parser.GetOutputChan())
		outputFile.Close()
	}
}
