package main

import (
	"fmt"
	"time"

	"bitbucket.org/ausocean/av/revid"
)

func main() {
	// Give the platform some time to set itself up
	time.Sleep(30 * time.Second)
	config := revid.Config{
		Input:            revid.Raspivid,
		Output:           revid.NativeRtmp,
		QuantizationMode: revid.QuantizationOff,
		RtmpUrl:          "rtmp://a.rtmp.youtube.com/live2/xt13-r4dh-f2w1-bh4s",
		Bitrate:          "500000",
		Packetization:    revid.Flv,
	}
	revidInst, err := revid.NewRevid(config)
	if err != nil {
		fmt.Println("Should not have got error!")
		return
	}
	// Run this instance for 2 days! Power to the pi will surely turn itself
	// off before this time is up.
	revidInst.Start()
	time.Sleep(2 * 43200 * time.Second)
	revidInst.Stop()
}
